SUMMARY = "Recipe for helloworld library"
DESCRIPTION = "Simple helloworld lib"
DEPENDS = ""
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=76dbe255ba9ea7b0058fd6d47fb76533"

BRANCH = "master"
SRCREV = "${AUTOREV}"

SRC_URI = "git://bitbucket.org/fmmsousa77/greetings_lib.git;branch=${BRANCH};protocol=ssh"

S = "${WORKDIR}/git"

inherit autotools

